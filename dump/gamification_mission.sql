-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: gamification
-- ------------------------------------------------------
-- Server version	5.6.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mission`
--

DROP TABLE IF EXISTS `mission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mission` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `application` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `point` decimal(18,2) NOT NULL,
  `forced` tinyint(1) DEFAULT NULL,
  `redirect` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mission`
--

LOCK TABLES `mission` WRITE;
/*!40000 ALTER TABLE `mission` DISABLE KEYS */;
INSERT INTO `mission` VALUES
(1,'ORBITA','Complete seus dados cadastrais','Para começar a sua jornada, nós queremos te conhecer melhor. A sua próxima missão é você nos contar um pouco mais sobre você.',100.00,1,'http://localhost:3000/producer/profile/edit'),
(2,'ORBITA','Cadastre o primeiro produto','Vamos iniciar a jornada de sucesso? A sua próxima missão é cadastrar o seu primeiro produto, é o primeiro passo rumo ao sucesso!',100.00,0,'http://localhost:3000/producer/product/newproduct'),
(3,'ORBITA','Divulgue seu novo produto','Agora que o seu produto está criado, a próxima missão é divulgá-lo para o seu público. Pegue os links de compartilhamento e divulgue o seu produto.',100.00,0,'http://localhost:3000/producer/campaigns'),
(4,'ORBITA','Aconteceu a primeira venda','Parabéns você começou a vender! Agora que estamos vendendo, vamos aumentar as vendas recuperando aqueles interessados que desistiram de comprar.',100.00,0,'http://localhost:3000/producer/myleads'),
(5,'ORBITA','Ative recuperação de vendas','Você notou que nem todos os clientes compram? A Eduzz tem um serviço de recuperação de vendas, sua missão agora é ativar a recuperação para converter ainda mais vendas.',100.00,1,NULL),
(6,'ORBITA','Acompanhe as vendas no Telescope','Agora que você está vendendo, nós conseguimos acompanhar os clientes comprando no checkout nesse momento. Sua missão agora é visualizar esse clientes no Telescope!',100.00,1,NULL),
(7,'ORBITA','Customize o seu checkout','Sabia que o checkout personalizado converte mais vendas? Sua missão agora é personalizar o seu checkout com a sua marca.',100.00,0,NULL),
(8,'ORBITA','Crie novos produtos','Você está indo muito bem! Para continuar a expandir o seu negócio, você deve criar novos produtos e continuar vendendo. Sua missão agora é criar um novo produto para vender.',100.00,0,'http://eduzz.com');
/*!40000 ALTER TABLE `mission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-01  0:35:55
