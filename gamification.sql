USE gamification;

CREATE TABLE profile(
  id bigint unsigned AUTO_INCREMENT,
  producer_id int,
  name varchar(255),
  avatar varchar(255),
  points float(18,2),
  level varchar(255),
  first_access boolean,
  last_version boolean,
  PRIMARY KEY (id)
);

CREATE TABLE mission(
  id bigint unsigned AUTO_INCREMENT,
  application varchar(50),
  title varchar(255),
  content varchar(255),
  point numeric(18,2),
  forced boolean,
  redirect varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE mission_status(
  id bigint unsigned AUTO_INCREMENT,
  producer_id int,
  mission_id bigint,
  completed boolean,
  PRIMARY KEY (id)
);

CREATE TABLE levels(
  id bigint unsigned AUTO_INCREMENT,
  title varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE levels_missions(
  level_id bigint,
  mission_id bigint,
  PRIMARY KEY (level_id, mission_id)
);

CREATE TABLE application_access (
  application varchar(50),
  first_access boolean,
  producer_id int
)