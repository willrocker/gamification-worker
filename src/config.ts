/* eslint-disable camelcase */

// Node Config
export const NODE_PORT = process.env.NODE_PORT || 4000;

// MYSQL - Database Config
export const DB_HOST = process.env.DB_HOST || 'mysql';
export const DB_USER = process.env.DB_USER || 'root';
export const DB_PASSWORD = process.env.DB_PASSWORD || 'admin';
export const DB_DATABASE = process.env.DB_DATABASE || 'gamification';
export const DB_PORT = process.env.DB_PORT || '3306';