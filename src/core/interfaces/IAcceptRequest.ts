export interface IAcceptRequest {
  mission: number;
  producer: number;
  accept: boolean;
}