import { IMissionDashboard } from './IMissionDashboard';

export interface IDashboard {
  avatar: string;
  name: string;
  points: number;
  level: string;
  progress: number;
  mission: IMissionDashboard [];
}