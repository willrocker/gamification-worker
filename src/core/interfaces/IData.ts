export interface IData {
  producerId:number,
  completedRegistry:boolean,
  productRegistered:boolean,
  disclosedProduct:boolean,
  firstSale:boolean,
  leads:boolean,
  recoveryActivated:boolean,
  checkoutCustomized:boolean,
  canCreateNewProducts:boolean
};