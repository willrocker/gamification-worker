export interface IMission {
  id: number;
  title: string;
  content: string;
  forced: boolean;
  redirect: string;
  point: number;
}