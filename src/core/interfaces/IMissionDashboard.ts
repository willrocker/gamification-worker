export interface IMissionDashboard {
  id: number;
  title: string;
  completed: number;
}