export interface IProfile {
  id: number,
  points: number,
  level: string,
  level_id: number
}