export interface IProfileDashboard {
  avatar: string;
  name: string;
  points: number;
  level: string;
  level_id: number;
}