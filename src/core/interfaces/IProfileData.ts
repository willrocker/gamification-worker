export interface IProfileData {
  id: number,
  producer_id: number,
  name: string,
  avatar:string,
  points: number,
  level: string,
  last_version: boolean
}