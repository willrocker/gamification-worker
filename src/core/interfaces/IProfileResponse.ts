export interface IProfileResponse {
  points: number,
  level: string,
  missions_level: number,
  missions_done: number
}