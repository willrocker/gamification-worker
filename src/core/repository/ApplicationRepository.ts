import db from '../../db';

class ApplicationRepository
{
  async getAccessByApplication(producer: number, application: string) {
    return db('application_access')
      .where('application', application)
      .where('producer_id', producer)
      .first();
  }

  async disable(producer: number, application:string) {
    return db('application_access')
      .insert({
        producer_id: producer,
        application: application,
        first_access: 1
      });
  }

}

export default new ApplicationRepository();
