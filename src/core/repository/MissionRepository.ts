import db from '../../db';
import { IMission } from '../interfaces/IMission';
import { IMissionDashboard } from '../interfaces/IMissionDashboard';
import {IMissionCount} from "../interfaces/IMissionCount";

class MissionRepository
{
  async getMission(producerId: number, application: string): Promise<IMission> {
    return db('profile')
      .select(
        'mission.id',
        'mission.title',
        'mission.content',
        'mission.forced',
        'mission.redirect',
        'mission.point'
      )
      .innerJoin('levels_missions', 'levels_missions.level_id', '=', 'profile.level')
      .innerJoin('mission_status', 'mission_status.mission_id', '=', 'levels_missions.mission_id')
      .innerJoin('mission', 'mission.id', '=', 'mission_status.mission_id')
      .where('profile.producer_id', producerId)
      .where('profile.last_version', 1)
      .where('mission_status.completed', 1)
      .where('mission.application', application)
      .first();
  }

  async updateMissionStatus(producer: number, mission: number, accept: number) {
    await db('mission_status')
      .where('producer_id', producer)
      .where('mission_id', mission)
      .update({
        completed: accept
      });
  }

  async findSpecificMission(producerId: number, missionId: number): Promise<IMission> {
    return db('mission')
      .innerJoin('mission_status', 'mission_status.mission_id', '=', 'mission.id')
      .where('mission_status.producer_id', producerId)
      .where('mission_status.mission_id', missionId)
      .first();
  }

  async getProducerMissionsActualLevel(producer: number, level: number): Promise<IMissionDashboard[]> {
    return db('mission')
      .select('mission.id', 'mission.title', 'mission_status.completed')
      .innerJoin('mission_status', 'mission_status.mission_id', '=', 'mission.id')
      .innerJoin('levels_missions', 'levels_missions.mission_id', '=', 'mission.id')
      .where('mission_status.producer_id', producer)
      .where('levels_missions.level_id', level);
  }

  async getMissionById(mission: number): Promise<IMission> {
    return db('mission')
      .where('id', mission)
      .first();
  }

  async all() {
    return db('mission')
      .select("*");
  }

  async getTotalMissionsByProducerLevel(producer: number, level:number): Promise<IMissionCount[]> {
    return db('levels_missions')
      .select('levels_missions.mission_id')
      .innerJoin('mission_status', 'mission_status.mission_id', '=', 'levels_missions.mission_id')
      .where('level_id', level)
      .where('producer_id', producer)
  }

  async getTotalMissionsDoneByProducerLevel(producer: number, level:number): Promise<IMissionCount[]> {
    return db('levels_missions')
      .select('levels_missions.mission_id')
      .innerJoin('mission_status', 'mission_status.mission_id', '=', 'levels_missions.mission_id')
      .where('level_id', level)
      .where('producer_id', producer)
      .where('mission_status.completed', 3);
  }
}

export default new MissionRepository();