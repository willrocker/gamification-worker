import db from '../../db';
import { IProfile } from '../interfaces/IProfile';
import { IProfileDashboard } from '../interfaces/IProfileDashboard';
import { IFirstAcess } from '../interfaces/IFirstAcess';
import { IProfileData } from '../interfaces/IProfileData';

class ProfileRepository
{
  async getProfile(producerId: number): Promise<IProfile> {
    return db('profile')
      .select(
        db.ref('profile.producer_id').as('id'),
        db.ref('profile.points').as('points'),
        db.ref('levels.title').as('level'),
        db.ref('levels.id').as('level_id'),
      )
      .innerJoin('levels', 'profile.level', '=', 'levels.id')
      .where('producer_id', producerId)
      .where('last_version', 1)
      .first();
  }

  async getProfileByProducerId(producer: number): Promise<IProfileData> {
    return db('profile')
      .where('producer_id', producer)
      .where('last_version', 1)
      .first();

  }

  async disable(id: number) {
    return db('profile')
      .where('id', id)
      .update({
        last_version: 0
      });
  }

  async newProfile(profile: IProfileData, score: number, level: number) {
    return db('profile')
      .insert({
        producer_id: profile.producer_id,
        name: profile.name,
        avatar: profile.avatar,
        points: score,
        level: level,
        last_version: 1
      })
  }


  async getProfileForDashBoard(producerId: number): Promise<IProfileDashboard> {
    return db('profile')
      .select(
        db.ref('profile.avatar').as('avatar'),
        db.ref('profile.name').as('name'),
        db.ref('profile.points').as('points'),
        db.ref('levels.title').as('level'),
        db.ref('levels.id').as('level_id')
      )
      .innerJoin('levels', 'profile.level', '=', 'levels.id')
      .where('producer_id', producerId)
      .first();
  }

  async isFirstAccess(producer: number): Promise<IFirstAcess> {
    return db('profile')
      .select('first_access')
      .where('producer_id', producer)
      .first();
  }
}

export default new ProfileRepository();