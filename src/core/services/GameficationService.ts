import ProfileRepository from '../repository/ProfileRepository';
import MissionRepository from '../repository/MissionRepository';
import ApplicationRepository from '../repository/ApplicationRepository';
import {IDashboard} from '../interfaces/IDashboard';
import {IProfileResponse} from '../interfaces/IProfileResponse';
import {IProfileData} from "../interfaces/IProfileData";

class GameficationService {
  async getProfile(producer: number): Promise<IProfileResponse>
  {
    const profile = await ProfileRepository.getProfile(producer);

    const mission = await MissionRepository.getProducerMissionsActualLevel(producer, profile.level_id);

    const missionsLevel = mission.length;

    let missionsDone = 0;

    for (const m of mission) {
      if (m.completed == 3) {
        missionsDone++;
      }
    }

    return {
      points: profile.points,
      level: profile.level,
      missions_level: missionsLevel,
      missions_done: missionsDone
    }
  }

  getMission(producerId: number, application: string)
  {
    return MissionRepository.getMission(producerId, application);
  }

  async dashboard(producer: number)
  {
     const profile = await ProfileRepository.getProfileForDashBoard(producer);
     const mission = await MissionRepository.getProducerMissionsActualLevel(producer, profile.level_id);

     let completed = 0;

     for (const m of mission) {
      if (m.completed == 3) {
        completed++;
      }
     }

     const progress = this.calculateProgress(mission.length, completed);

     const dashboard: IDashboard = {
       avatar: profile.avatar,
       name: profile.name,
       points: profile.points,
       level: profile.level,
       progress: progress,
       mission: mission
     };

     return dashboard;
  }

  calculateProgress(size: number, completed: number): number {
    return Math.round(((completed / size ) * 100));
  }

  async updateMissionStatus(
    producer: number,
    mission: number,
    accept: boolean
  ) {

    const acceptMission = accept ? 3 : 2;

    await MissionRepository.updateMissionStatus(
      producer,
      mission,
      acceptMission
    );

    if (acceptMission == 3) {
      const profile = await ProfileRepository.getProfileByProducerId(producer);

      const score = await this.processScoring(mission, profile);

      const pro = await ProfileRepository.getProfile(producer);

      const missions =  await MissionRepository.getTotalMissionsByProducerLevel(producer, pro.level_id);

      const numMissions = missions ? missions.length : 0;

      const missionsDone = await MissionRepository.getTotalMissionsDoneByProducerLevel(producer, pro.level_id);

      const numMissionsDone = missionsDone ? missionsDone.length : 0;

      let level = pro.level_id;

      if (numMissions == numMissionsDone) {
        level = 2;
      }

      await ProfileRepository.newProfile(profile, score, level);

      await ProfileRepository.disable(profile.id);
    }
  }

  findSpecificMission(missionId: number, producerId: number) {
    return MissionRepository.findSpecificMission(producerId, missionId);
  }

  async getWelcomeMessage(producer: number, application: string) {
    if (application == "TELESCOPE") {
      const applicationAccess = await ApplicationRepository.getAccessByApplication(producer, application);

      if (!applicationAccess) {

        await ApplicationRepository.disable(producer, application);
        return {
          message: "Este é o telescope. Nele você pode acompanhar as suas vendas em tempo real e tomar decisões estratégicas para melhorar a performance delas"
        };
      }
    }

    if (application == "ORBITA") {
      const applicationAccess = await ApplicationRepository.getAccessByApplication(producer, application);
      if (!applicationAccess) {
        await ApplicationRepository.disable(producer, application);
        return {
          message: "Seja bem-vindo à Eduzz! Eu vou te ajudar a explorar todo o potencial que a Eduzz tem para te ajudar a crescer o seu negócio. Cada avanço você vai ganhando pontos!"
        };
      }
    }

    return null;
  }

  allMissions() {
    return MissionRepository.all();
  }

  async processScoring(mission: number, profile: IProfileData): Promise<number> {
    const missionData = await MissionRepository.getMissionById(mission);

    return Math.round(profile.points + missionData.point);
  }
}

export default new GameficationService();