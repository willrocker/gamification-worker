import Knex from 'knex';
import { DB_HOST, DB_DATABASE, DB_USER, DB_PASSWORD, DB_PORT } from './config';

const knex = Knex({
  client: 'mysql',
  connection: {
    host: DB_HOST,
    database: DB_DATABASE,
    user: DB_USER,
    password: DB_PASSWORD,
    port: Number(DB_PORT),
    connectionTimeout: 60000,
    requestTimeout: 60000
  },
  debug: false
});

export default knex;
