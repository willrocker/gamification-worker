import express from 'express';
import http from 'http';
import socket from 'socket.io';
import {logger} from './core/support/logger';
import GameficationService from './core/services/GameficationService';

//Port from environment variable or default - 4001
const port = process.env.PORT || 4000;

//Setting up express and adding socketIo middleware
const app = express();
const server = http.createServer(app);
const io = socket(server);

//Setting up a socket with the namespace "connection" for new sockets
io.on("connection", socket => {
  logger.info("New client connected");

  socket.on("welcome", (data) => {
    GameficationService
      .getWelcomeMessage(data.producer, data.application)
      .then(result => {
        if (result) {
          socket.emit("welcome", result);
        }
      });
  });

  setInterval(() => {
    GameficationService
      .getMission(6, "ORBITA")
      .then(result => {
        if (result) {
          socket.emit("mission", result);
        }
      });
  }, 5000);

  socket.on("profile", (data) => {
    GameficationService
      .getProfile(data.producer)
      .then(result => {
        socket.emit("profile", result);
      });
  });

  socket.on("mission", (data) => {
    GameficationService
      .getMission(data.producer, data.application)
      .then(result => {
        if (result) {
          socket.broadcast.emit("mission", result);
        }
      });
  });

  socket.on("accept", (data) => {
    console.log('olha a cagada');
    GameficationService.updateMissionStatus(
      data.producer,
      data.mission,
      data.accept
    ).then( () => {
      GameficationService
        .getProfile(data.producer)
        .then(result => {
          console.log('Produtor', data.producer);
          console.log('Resultado Profile:', result);
          socket.emit("profile", result);
        });
    });

    console.log('que porra');
  });

  socket.on("dashboard", (data) => {
    GameficationService
      .dashboard(data.producer)
      .then(result => {
        socket.emit("dashboard", result)
      });
  });

  socket.on("event", (data) => {
    GameficationService
      .findSpecificMission(data.mission, data.producer)
      .then(result => {
        socket.emit("event", result);
      });
  });

  socket.on("all-missions", () => {
    GameficationService
      .allMissions()
      .then(result => {
        socket.emit("all-missions", result);
      });
  });
});

server.listen(port, () => logger.info(`Listening on port ${port}`));